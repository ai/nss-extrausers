# Makefile for nss-extrausers

CC = gcc
prefix = /usr
exec_prefix = ${prefix}
libprefix = ${exec_prefix}/lib
DESTDIR=
OBJECTS=shadow.o passwd.o group.o
SHARED_OBJECT = libnss_extrausers.so.2
CFLAGS = -g -O2 -Wall -Wstrict-prototypes -Wpointer-arith -Wmissing-prototypes
LDFLAGS= -shared -Wl,-soname,$(SHARED_OBJECT) -Wl,-z,defs

all: $(SHARED_OBJECT)

$(SHARED_OBJECT): $(OBJECTS)
	gcc $(CFLAGS) $(LDFLAGS) -o $(SHARED_OBJECT) $(OBJECTS) -lc

shadow.o: shadow.c s_config.h
	$(CC) $(CFLAGS) -fPIC -c -o shadow.o shadow.c
group.o: group.c s_config.h
	$(CC) $(CFLAGS) -fPIC -c -o group.o group.c
passwd.o: passwd.c s_config.h
	$(CC) $(CFLAGS) -fPIC -c -o passwd.o passwd.c

install:
	install -m644 $(SHARED_OBJECT) $(DESTDIR)$(libprefix)/

clean:
	rm -f $(OBJECTS)
	rm -f $(SHARED_OBJECT)

distclean: clean
