/*
    Copyright (C) 2001,2002 Bernhard R. Link <brlink@debian.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    Please tell me, if you find errors or mistakes.

Based on parts of the GNU C Library:

   Common code for file-based database parsers in nss_files module.
   Copyright (C) 1996, 1997, 1998, 1999, 2000 Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.
*/

#define _GNU_SOURCE 1

#include <stdlib.h>
#include <stdio.h>
#include <nss.h>
#include <string.h>
#include <shadow.h>
#include <time.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <ctype.h>

#include "s_config.h"

enum nss_status _nss_extrausers_getspnam_r (const char *, struct spwd *,char *, size_t,int *);

enum nss_status _nss_extrausers_getspnam_r (const char *name, struct spwd *spw,
                char *buffer, size_t buflen,int * errnop)
{
#define SANEQUIT {funlockfile(stream);fclose(stream);}
#define CHECKCOLON if(*p != ':' ) { \
				SANEQUIT \
				*errnop = 0; \
				return NSS_STATUS_UNAVAIL; \
			} else { \
			*(p++) = '\0'; \
			}
#define TOCOLON(p,h) { while( *p && *p != ':' ) p++; CHECKCOLON }
	FILE *stream;
	char *p,*h;
	char *t_namp,*t_pwdp;
	long int t_lstchg,t_min,t_max,t_warn,t_inact,t_expire;
	unsigned long int t_flag;

	if ( spw == NULL || name == NULL )
	{
		*errnop = EPERM;
		return NSS_STATUS_UNAVAIL;
	}

	stream = fopen(SHADOWFILE,"r");
	if( stream == NULL ) {
		*errnop = errno;
		return NSS_STATUS_UNAVAIL;
	}
	flockfile(stream);
	while( 1 ) {
		buffer[buflen - 1] = '\xff';
		p = fgets_unlocked(buffer,buflen,stream);
		if( p == NULL ) {
			if( feof_unlocked(stream) ) {
				SANEQUIT
				*errnop = ENOENT;
				return NSS_STATUS_NOTFOUND;
			} else {
				*errnop = errno;
				SANEQUIT
				return NSS_STATUS_UNAVAIL;
			}
		}
		h = index(p,'\n');
		if( buffer[buflen - 1] != '\xff' || h == NULL ) {
			SANEQUIT
			*errnop = ERANGE;
			return NSS_STATUS_TRYAGAIN;
		}
		while( isspace(*h) && h >= p) {
			*h = '\0';
			h--;
		}

		/* extract name */
		while (isspace (*p))
			++p;
		t_namp = p;
		TOCOLON(p,h);
		if( name && strcmp(name,t_namp)!=0 )
			continue;
		/* passwd */
		while (isspace (*p))
			++p;
		t_pwdp = p;
		TOCOLON(p,h);
		/* extract day of last changes */
		t_lstchg = strtol(p,&h,10);
		p=h;
		CHECKCOLON;
		p = ++h;
		/* extract min */
		t_min = strtol(p,&h,10);
		p=h;
		CHECKCOLON;
		/* extract max */
		t_max = strtol(p,&h,10);
		p=h;
		CHECKCOLON;
		/* extract days of warning */
		t_warn = strtol(p,&h,10);
		p=h;
		CHECKCOLON;
		/* extract days of inactivity */
		t_inact = strtol(p,&h,10);
		p=h;
		CHECKCOLON;
		/* extract day of expire */
		t_expire = strtol(p,&h,10);
		p=h;
		CHECKCOLON;
		/* extract reserved flags */
		t_flag = strtoul(p,&h,10);
		if( *h != '\0' ) {
			SANEQUIT
			*errnop = 0;
			return NSS_STATUS_UNAVAIL;
		}
		SANEQUIT
		*errnop = 0;
		spw->sp_namp = t_namp;
		spw->sp_pwdp = t_pwdp;
		spw->sp_lstchg = t_lstchg;
		spw->sp_min = t_min;
		spw->sp_max = t_max;
		spw->sp_warn = t_warn;
		spw->sp_inact = t_inact;
		spw->sp_expire = t_expire;
		spw->sp_flag = t_flag;
		return NSS_STATUS_SUCCESS;
	}
}
